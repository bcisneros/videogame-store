<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html ng-app="store">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Videogame Store</title>
        <link href="<c:url value='/static/images/favicon.ico' />" rel="icon" type="image/x-icon" />
        <link href="<c:url value='/static/css/vendor/bootstrap.min.css' />" rel="stylesheet"/>
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
    </head>
    <body>
        <header class="container">
            <h1><a href="<c:url value='/' />">Videogame Store</a></h1>
        </header>

        <section id="main" class="container" ng-view>
        </section>

        <footer>
            <address class="container">Benjamin Cisneros (benjamin.cisneros at softtek.com)</address>
        </footer>
        <script src="<c:url value='/static/js/vendor/angular.js' />"></script>
        <script src="<c:url value='/static/js/vendor/angular-route.min.js' />"></script>
        <script src="<c:url value='/static/js/app.js' />"></script>
        <script src="<c:url value='/static/js/service/review_service.js' />"></script>
        <script src="<c:url value='/static/js/service/videogame_service.js' />"></script>
        <script src="<c:url value='/static/js/controller/videogame_controller.js' />"></script>  
    </body>
</html>

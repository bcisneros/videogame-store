<section>
    <h3>Reviews</h3>
    <article ng-repeat="review in VideogameCtrl.videogame.reviews">
        <blockquote>{{review.comment}} <cite>by {{review.reviewerName}}</cite>
            <span>Published at {{review.creationDate| date:'d-MMM-yyyy'}}</span>
        </blockquote>
    </article>
    <div>
        <a class="btn-block" href="#/videogames/{{VideogameCtrl.videogame.id}}/reviews/new" >Add new Review</a>
    </div>
</section>

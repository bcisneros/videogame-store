<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
    <input type="search" placeholder="Search Video game by name or description" class="form-control" ng-model="query"/>
</section>
<article ng-repeat="videogame in videogames = (VideogameCtrl.videogames | filter:query | orderBy: ratingAverage)">
    <h2><a ng-href="#/videogames/{{videogame.id}}">{{videogame.name}}</a></h2>
    <p>{{videogame.description}}</p>
    <p>Rating: <em>{{videogame.ratingAverage}}</em></p>
    <a href="#"><img ng-src="<c:url value="/static" />{{videogame.image}}" /></a>
</article>
<div ng-show="videogames.length <= 0" class="no-results">No video games match with the criteria.</div>
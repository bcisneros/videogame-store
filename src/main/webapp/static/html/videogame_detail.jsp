<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<section id="main" ng-init="VideogameCtrl.retrieveVideogame()">
    <h2>{{VideogameCtrl.videogame.name}}</h2>
    <p>{{VideogameCtrl.videogame.description}}</p>
    <p>Rating: <em>{{VideogameCtrl.videogame.ratingAverage}}</em></p>
    <img ng-src="<c:url value="/static" />{{VideogameCtrl.videogame.image}}" />
    <reviews-list></reviews-list>
</section>

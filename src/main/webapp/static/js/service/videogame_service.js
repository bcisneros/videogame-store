/* global angular */

'use strict';

angular.module('store')
        .factory('VideogameService', ['$http', '$q', function ($http, $q) {
                return {
                    listAllVideogames: function () {
                        return $http.get('http://localhost:8080/videogame-store/videogames/')
                                .then(function (response) {
                                    return response.data;
                                }, function (errResponse) {
                                    console.error("Error while retrieving all the videogames");
                                    $q.reject(errResponse);
                                });
                    },
                    retrieveVideogameById: function (id) {
                        return $http.get('http://localhost:8080/videogame-store/videogames/' + id)
                                .then(function (response) {
                                    return response.data;
                                }, function (errResponse) {
                                    console.error("Error while retrieving all the videogames");
                                    $q.reject(errResponse);
                                });
                    }
                };
            }]);
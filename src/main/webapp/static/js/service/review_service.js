/* global angular */

'use strict';

angular.module('store')
        .factory('ReviewService', ['$http', '$q', function ($http, $q) {
                return {
                    retrieveReviewsByVideogameId: function (id) {
                        return $http.get('http://localhost:8080/videogame-store/videogames/' + id + '/reviews')
                                .then(function (response) {
                                    return response.data;
                                }, function (errResponse) {
                                    console.error("Error while retrieving all the reviews for videogame with id = " + id);
                                    $q.reject(errResponse);
                                });
                    }
                };
            }]);



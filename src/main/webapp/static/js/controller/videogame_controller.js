/* global angular */

'use strict';
angular.module('store')
        .controller('VideogameController', ['VideogameService', 'ReviewService', '$routeParams',
            function (VideogameService, ReviewService, $routeParams) {
                var self = this;
                self.videogames = [];
                self.videogame = {};
                self.id = 0;

                self.listAllVideogames = function () {
                    VideogameService.listAllVideogames()
                            .then(
                                    function (d) {
                                        self.videogames = d;
                                    },
                                    function (errResponse) {
                                        console.error('Error while fetching Videogames');
                                        console.error(errResponse);
                                    }
                            );
                };

                self.retrieveVideogame = function () {
                    self.videogame = {};
                    VideogameService.retrieveVideogameById($routeParams.id)
                            .then(
                                    function (d) {
                                        self.videogame = d;
                                        ReviewService.retrieveReviewsByVideogameId(self.videogame.id)
                                                .then(
                                                        function (d) {
                                                            self.videogame.reviews = d;
                                                        },
                                                        function (errResponse) {
                                                            console.error('Error while fetching Videogames');
                                                            console.error(errResponse);
                                                        });
                                    },
                                    function (errResponse) {
                                        console.error('Error while fetching Videogame with id: ' + $routeParams.id);
                                        console.error(errResponse);
                                    }
                            );
                    console.log($routeParams.id);
                };

                self.listAllVideogames();
            }]);


/* global angular */

'use strict';
angular.module('store', ['ngRoute']);

angular.module('store')
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider
                        .when('/videogames',
                                {
                                    templateUrl: '/videogame-store/static/html/videogames_list.jsp',
                                    controller: 'VideogameController',
                                    controllerAs: 'VideogameCtrl'
                                }
                        )
                        .when('/videogames/:id',
                                {
                                    templateUrl: '/videogame-store/static/html/videogame_detail.jsp',
                                    controller: 'VideogameController',
                                    controllerAs: 'VideogameCtrl'
                                })
                        .when('/videogames/:id/reviews/new',
                                {
                                    templateUrl: '/videogame-store/static/html/add_review_form.jsp'
                                })
                        .otherwise(
                                {
                                    redirectTo: '/videogames'
                                }
                        );
            }])
        .directive('reviewsList', function () {
            return {
                restrict: 'E',
                templateUrl: '/videogame-store/static/html/reviews_list.jsp',
                controller: function () {
                    this.setReviews = function (reviews) {
                        this.reviews = reviews;
                    }
                },
                controllerAs: 'ReviewCtrl'
            };
        });

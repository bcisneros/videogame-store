package com.softtek.videogamestore.repository;

import com.softtek.videogamestore.model.Videogame;
import java.util.List;

/**
 * Define all the actions you can perform in a VideogameRepository
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
public interface VideogameRepository {

    /**
     * Retrieves all the videogames
     * @return A {@code List} object with all videogames stored
     */
    List<Videogame> fetchAll();

    public Videogame retrieveById(long id);
}

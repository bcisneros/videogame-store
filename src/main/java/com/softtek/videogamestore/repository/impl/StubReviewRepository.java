package com.softtek.videogamestore.repository.impl;

import com.softtek.videogamestore.model.Review;
import com.softtek.videogamestore.repository.ReviewRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implements ReviewRepository for test purposes
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
@Repository("reviewRepository")
@Transactional
public class StubReviewRepository implements ReviewRepository {

    private final static List<Review> dummyReviews;

    static {
        dummyReviews = createDummyReviews();
    }

    @Override
    public List<Review> retrieveAllReviewsByVideogameId(long id) {
        List<Review> reviews = new ArrayList<>();
        for (Review review : dummyReviews) {
            if (id == review.getVideogameId()) {
                reviews.add(review);
            }
        }
        return reviews;
    }

    private static List<Review> createDummyReviews() {
        List<Review> tempReviews = new ArrayList<>();
        Review reviewOne = new Review();
        reviewOne.setId(1);
        reviewOne.setComment("Excellent video game. One of my favorites");
        reviewOne.setVideogameId(1);
        reviewOne.setCreationDate(new Date());
        reviewOne.setRate(5);
        reviewOne.setReviewerEmail("test@test.com");
        tempReviews.add(reviewOne);
        return tempReviews;
    }

}

package com.softtek.videogamestore.repository.impl;

import com.softtek.videogamestore.model.Videogame;
import com.softtek.videogamestore.repository.VideogameRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Create a Stub implementation of {@code VideogameRepository} interface
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
@Repository("videogameRepository")
@Transactional
public class StubVideogameRepository implements VideogameRepository {

    private static final List<Videogame> dummyVideogames;

    static {
        dummyVideogames = createDummyVideogames();
    }

    @Override
    public List<Videogame> fetchAll() {
        return dummyVideogames;
    }

    private static List<Videogame> createDummyVideogames() {
        List<Videogame> tempVideogames = new ArrayList<>();

        Videogame gameOne = new Videogame();
        gameOne.setId(1L);
        gameOne.setName("Super Mario Bros");
        gameOne.setDescription("Super Mario Bros. is a 1985 platform video game internally developed by Nintendo R&D4 and published by Nintendo as a pseudo-sequel to the 1983 game Mario Bros. It was originally released in Japan for the Family Computer on September 13, 1985, and later that year for the Nintendo Entertainment System in North America and Europe on May 15, 1987, and Australia later in 1987. It is the first of the Super Mario series of games. In Super Mario Bros., the player controls Mario and in a two-player game, a second player controls Mario's brother Luigi as he travels through the Mushroom Kingdom in order to rescue Princess Toadstool from the antagonist Bowser.");
        gameOne.setImage("/images/super_mario_bros.png");

        Videogame gameTwo = new Videogame();
        gameTwo.setId(2L);
        gameTwo.setName("Donkey Kong Country");
        gameTwo.setDescription("Donkey Kong Country is a 1994 platforming video game developed by Rare and published by Nintendo for the Super Nintendo Entertainment System. It was first released in November 1994, and under the name Super Donkey Kong in Japan. The game was later re-released for the Game Boy Color (2000), Game Boy Advance (2003), Wii Virtual Console (2007), and Wii U Virtual Console (2014).");
        gameTwo.setImage("/images/donkey_kong_country.png");

        Videogame gameThree = new Videogame();
        gameThree.setId(3L);
        gameThree.setName("The Legend of Zelda: Ocarina of Time");
        gameThree.setDescription("The Legend of Zelda: Ocarina of Time is a 1998 action-adventure video game developed by Nintendo's Entertainment Analysis & Development division for the Nintendo 64 home video game console. It was released in Japan and North America in November 1998, and in Europe and Australia in December 1998. Originally developed for the 64DD peripheral,[11] the game was instead released on a 256-megabit (32-megabyte) cartridge, the largest-capacity cartridge Nintendo produced at that time. Ocarina of Time is the fifth game in the The Legend of Zelda series, and the first with 3D graphics. It was followed 18 months later by the direct sequel, The Legend of Zelda: Majora's Mask.");
        gameThree.setImage("/images/zelda_ocarina_of_time.png");

        tempVideogames.add(gameOne);
        tempVideogames.add(gameTwo);
        tempVideogames.add(gameThree);

        return tempVideogames;
    }

    @Override
    public Videogame retrieveById(long id) {
        for (Videogame videogame : dummyVideogames) {
            if (videogame.getId() == id) {
                return videogame;
            }
        }
        return null;
    }

}

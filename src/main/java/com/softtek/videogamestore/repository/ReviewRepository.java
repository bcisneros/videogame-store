package com.softtek.videogamestore.repository;

import com.softtek.videogamestore.model.Review;
import java.util.List;

/**
 * Define all the actions you can perform in a Review Repository
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
public interface ReviewRepository {

    /**
     * Retrieves all the reviews with a given video game id
     *
     * @param id The identifier of the video game
     * @return A list of reviews if exists or empty list if the video game does
     * not have any review
     */
    public List<Review> retrieveAllReviewsByVideogameId(long id);
}

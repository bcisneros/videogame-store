package com.softtek.videogamestore.model;

/**
 * Represents a Videogame object
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
public class Videogame {

    private long id;
    private String name;
    private String description;
    private String image;
    private float ratingAverage;

    public float getRatingAverage() {
        return ratingAverage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

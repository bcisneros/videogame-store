package com.softtek.videogamestore.model;

import java.util.Date;

/**
 * Represents a Review object
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
public class Review {

    private long id;
    private String comment;
    private int rate;
    private Date creationDate;
    private long videogameId;
    private String reviewerEmail;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public long getVideogameId() {
        return videogameId;
    }

    public void setVideogameId(long videogameId) {
        this.videogameId = videogameId;
    }

    public String getReviewerEmail() {
        return reviewerEmail;
    }

    public void setReviewerEmail(String reviewerEmail) {
        this.reviewerEmail = reviewerEmail;
    }

}

package com.softtek.videogamestore.service;

import com.softtek.videogamestore.model.Review;
import java.util.List;

/**
 * Define all methods required for a Review Service
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
public interface ReviewService {

    public List<Review> retrieveAllReviewsByVideogameId(long id);
}

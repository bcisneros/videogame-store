package com.softtek.videogamestore.service;

import com.softtek.videogamestore.model.Videogame;
import java.util.List;

/**
 * Define all methods required for a VideogameService
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
public interface VideogameService {

    /**
     * Retrieves all the video games
     *
     * @return
     */
    public List<Videogame> fetchAll();

    /**
     * Retrieves a video game using the video game identifier
     *
     * @param id The video game identifier
     * @return A {@code Videogame} object if exists or null in other case
     */
    public Videogame retrieveById(long id);
}

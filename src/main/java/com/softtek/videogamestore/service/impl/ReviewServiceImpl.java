package com.softtek.videogamestore.service.impl;

import com.softtek.videogamestore.model.Review;
import com.softtek.videogamestore.repository.ReviewRepository;
import com.softtek.videogamestore.service.ReviewService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description of the class ReviewServiceImpl
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
@Service("reviewService")
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    ReviewRepository reviewRepository;

    @Override
    public List<Review> retrieveAllReviewsByVideogameId(long id) {
        return reviewRepository.retrieveAllReviewsByVideogameId(id);
    }

}

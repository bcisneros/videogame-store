package com.softtek.videogamestore.service.impl;

import com.softtek.videogamestore.model.Videogame;
import com.softtek.videogamestore.repository.VideogameRepository;
import com.softtek.videogamestore.service.VideogameService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implements {@code VideogameService} interface
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
@Service("videogameService")
public class VideogameServiceImpl implements VideogameService {

    @Autowired
    VideogameRepository videogameRepository;

    @Override
    public List<Videogame> fetchAll() {
        return videogameRepository.fetchAll();
    }

    @Override
    public Videogame retrieveById(long id) {
        return videogameRepository.retrieveById(id);
    }

}

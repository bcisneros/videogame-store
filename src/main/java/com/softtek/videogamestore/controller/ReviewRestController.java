package com.softtek.videogamestore.controller;

import com.softtek.videogamestore.model.Review;
import com.softtek.videogamestore.service.ReviewService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description of the class ReviewRestController
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
@RestController
public class ReviewRestController {

    @Autowired
    private ReviewService reviewService;

    @RequestMapping(value = "/videogames/{id}/reviews", method = RequestMethod.GET)
    public ResponseEntity<List<Review>> getVideogame(@PathVariable("id") long id) {
        List<Review> reviews = reviewService.retrieveAllReviewsByVideogameId(id);
        if (reviews.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(reviews, HttpStatus.OK);
    }
}

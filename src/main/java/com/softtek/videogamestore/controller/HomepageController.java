package com.softtek.videogamestore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * The controller for the Home page
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
@Controller
public class HomepageController {

    /**
     * Process a GET request and return the home page
     *
     * @return A reference to the home page
     */
    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String homePage() {
        return "home";
    }
}

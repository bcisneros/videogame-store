package com.softtek.videogamestore.controller;

import com.softtek.videogamestore.model.Videogame;
import com.softtek.videogamestore.service.VideogameService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller to access at services related with videogames
 *
 * @author Benjamin Cisneros <benjamin.cisneros@softtek.com>
 */
@RestController
public class VideogameRestController {

    @Autowired
    private VideogameService videogameService;

    @RequestMapping(value = "/videogames/", method = RequestMethod.GET)
    public ResponseEntity<List<Videogame>> listAllCars() {
        List<Videogame> videogames = videogameService.fetchAll();
        if (videogames.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(videogames, HttpStatus.OK);
    }

    @RequestMapping(value = "/videogames/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Videogame> getVideogame(@PathVariable("id") long id) {
        Videogame videogame = videogameService.retrieveById(id);
        if (videogame == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(videogame, HttpStatus.OK);
    }
}
